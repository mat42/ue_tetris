// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyTetrisGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MYTETRIS_API AMyTetrisGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AMyTetrisGameModeBase();
};
