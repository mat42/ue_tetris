// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <functional>

#include "CoreMinimal.h"
#include "Square.h"
#include "GameFramework/Actor.h"
#include "Blocks.generated.h"

UCLASS()
class MYTETRIS_API ABlocks : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlocks();

	UPROPERTY(EditAnywhere, Category="Parameters")
	float SquareSide;
	
	UPROPERTY(EditAnywhere, Category="Parameters")
	int FieldWidth;
	
	UPROPERTY(EditAnywhere, Category="Parameters")
	int FieldHeight;

	UPROPERTY(EditAnywhere, Category="Parameters")
	float RowsDestroyTimeout;

	UPROPERTY(EditAnywhere, Category="Dependencies")
	TSubclassOf<ASquare> SquareActor;

	UPROPERTY(EditAnywhere, Category="Dependencies")
	TArray<UMaterial*> SquareMaterials;

	UPROPERTY(EditAnywhere, Category="Dependencies")
	UMaterial* SquareDestroyMaterial;

	UPROPERTY(BlueprintReadOnly, Category="UI")
	bool IsGameOver;
	
	UPROPERTY(BlueprintReadOnly, Category="UI")
	int Score;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
private:
	void GenerateBlock();
	
	bool TestCollision(std::function<FVector(const FVector&)> Transform, float BoxSide=1);

	bool CheckFilledRows();

	void DestroyRows();

public:
	bool MoveDown();
	
	void MoveLeftRight(float AxisValue);
	
	void Rotate();

	void Restart();

private:
	UPROPERTY()
	TArray<ASquare*> Blocks;
	FVector StartupLocation;
	FRotator StartupRotation;
	TArray<FOverlapResult> SquaresToDestroy;
	TArray<std::function<void()>> DropFunctions;
	int DestroyedRowsCount;
	float RowsDestroyTimer;
};
