// Fill out your copyright notice in the Description page of Project Settings.


#include "Blocks.h"

#include <thread>

#include "ChaosInterfaceWrapperCore.h"
#include "EngineUtils.h"
#include "Field/FieldSystemNoiseAlgo.h"
#include "Kismet/GameplayStatics.h"
#include "Windows/WindowsApplication.h"

// Sets default values
ABlocks::ABlocks()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SquareSide = 100.f;
	FieldWidth = 20;
	FieldHeight = 30;
	RowsDestroyTimeout = 0.5;
	RowsDestroyTimer = RowsDestroyTimeout;
	IsGameOver = false;
	Score = 0;
}

// Called when the game starts or when spawned
void ABlocks::BeginPlay()
{
	Super::BeginPlay();
	StartupLocation = GetActorLocation();
	StartupRotation = GetActorRotation();
	GenerateBlock();
}

// Called every frame
void ABlocks::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(SquaresToDestroy.Num() != 0)
	{
		RowsDestroyTimer -= DeltaTime;
	}
	if(RowsDestroyTimer <= 0)
	{
		DestroyRows();
		GenerateBlock();
		RowsDestroyTimer = RowsDestroyTimeout;
		Score += DestroyedRowsCount;
	}
}

void ABlocks::GenerateBlock()
{
	std::pair<int, int> Configurations[][4] = {
		{{0, 0},{1, 0},{2, 0},{3, 0}},
		{{0, 0},{1, 0},{2, 0},{2, 1}},
		{{0, 0},{0, 1},{1, 0},{2, 0}},
		{{0, 0},{0, 1},{1, 0},{1, 1}},
		{{0, 0},{0, 1},{1, 1},{1, 2}},
		{{0, 1},{1, 1},{1, 0},{2, 0}},
		{{0, 1},{1, 1},{1, 0},{2, 1}}
	};
	SetActorLocation(StartupLocation + FVector(0, FMath::RandRange(4, FieldWidth - 4) * SquareSide, 0));
	SetActorRotation(StartupRotation);
	UMaterial *Mat = SquareMaterials[FMath::RandRange(0, SquareMaterials.Num() - 1)];
	const int RandInd = FMath::RandRange(0, 6);
	for(const auto &Offset : Configurations[RandInd])
	{
		ASquare* Block = GetWorld()->SpawnActor<ASquare>(SquareActor, GetActorLocation(),FRotator(0.f));
		Block->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		Block->SetActorRelativeLocation(FVector(0,Offset.first * SquareSide - 0.5 * SquareSide,
			-Offset.second * SquareSide + 0.5 * SquareSide));
		Block->Mesh->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
		if(SquareMaterials.Num() != 0)
		{
			Block->Mesh->SetMaterial(0, Mat);
		}
		Block->SetActorEnableCollision(true);
		Blocks.Add(Block);
	}
}

void ABlocks::MoveLeftRight(float AxisValue)
{
	FVector Offset = FVector(0.f, AxisValue * SquareSide, 0.f);
	FVector OffsetDown = FVector(0.f, 0.f,  -SquareSide);
	if(!TestCollision([&Offset](const FVector& L)->FVector{return L + Offset;}))
	{
		SetActorLocation(GetActorLocation() + Offset);
	}
}

bool ABlocks::MoveDown()
{
	FVector Offset = FVector(0.f, 0.f,  -SquareSide);
	if(!TestCollision([&Offset](const FVector& L)->FVector{return L + Offset;}))
	{
		SetActorLocation(GetActorLocation() + Offset);
		return true;
	}
	for(const auto& Block : Blocks)
	{
		Block->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	}
	Blocks.Empty();
	if(GetActorLocation().Z == StartupLocation.Z)
	{
		UE_LOG(LogTemp, Warning, TEXT("Game Over!"));
		IsGameOver = true;
	}else if(!CheckFilledRows()){
		GenerateBlock();
	}
	return false;
}

void ABlocks::Rotate()
{
	if(!TestCollision([this](const FVector& L)->FVector
	{
		const auto B = GetActorLocation();
		return B + (L - B).RotateAngleAxis(-90, FVector(1,0,0));
	}))
	{
		SetActorRelativeRotation(GetActorRotation() + FRotator(0, 0, 90));
	}
}

void ABlocks::Restart()
{
	for(TActorIterator<ASquare> It(GetWorld()); It; ++It)
		(*It)->Destroy();
	Score = 0;
	IsGameOver = false;
	GenerateBlock();
}

bool ABlocks::TestCollision(std::function<FVector(const FVector&)> Transform, float BoxSide)
{
	FCollisionQueryParams Params;
	for(const AActor *Block : Blocks) Params.AddIgnoredActor(Block);
	for(const ASquare *Block : Blocks)
	{
		FVector TPos = Transform(Block->GetActorLocation());
		const int Ymax = StartupLocation.Y + FieldWidth * SquareSide;
		const int Zmin = StartupLocation.Z - FieldHeight * SquareSide;
		if(TPos.Y > Ymax || TPos.Y < StartupLocation.Y || TPos.Z < Zmin)
		{
			return true;
		}
		FCollisionShape CShape;
		CShape.SetBox(FVector(BoxSide));
		if(GetWorld()->OverlapAnyTestByObjectType(TPos, Block->GetActorQuat(),
			ECC_WorldDynamic, CShape, Params))
		{
			return true;
		}
	}
	return false;
}

bool ABlocks::CheckFilledRows()
{
	FCollisionShape CShape;
	CShape.SetBox(FVector(1, FieldWidth * SquareSide / 2, 1));
	int R = FieldHeight;
	int NRowsRemoved = 0;
	for(; R >= 0; --R)
	{
		TArray<FOverlapResult> OutSquaresRow;
		GetWorld()->OverlapMultiByObjectType(OutSquaresRow,
			StartupLocation + FVector(0,FieldWidth * SquareSide * 0.5f,-SquareSide * R + SquareSide * 0.5f),
			StartupRotation.Quaternion(),ECC_WorldDynamic, CShape);
		const bool RIsFull = OutSquaresRow.Num() == FieldWidth;
		const bool RIsEmpty = OutSquaresRow.Num() == 0;
		if(RIsFull)
		{
			SquaresToDestroy.Append(OutSquaresRow);
			NRowsRemoved++;
		}else if(RIsEmpty)
		{
			break;
		}else if(NRowsRemoved != 0)
		{
			const float ZOffset = -NRowsRemoved * SquareSide;
			DropFunctions.Add([OutSquaresRow, ZOffset]()->void
			{
				for(const auto &Overlap : OutSquaresRow)
					Overlap.Actor->SetActorLocation(Overlap.Actor->GetActorLocation() + FVector(0,0, ZOffset));
			});
		}
	}
	for(const auto& S : SquaresToDestroy)
	{
		ASquare *Square = reinterpret_cast<ASquare *>(S.Actor.Get());
		if(Square && SquareDestroyMaterial) Square->Mesh->SetMaterial(0, SquareDestroyMaterial);
	}
	DestroyedRowsCount = NRowsRemoved;
	return SquaresToDestroy.Num() != 0;
}

void ABlocks::DestroyRows()
{
	TArray<AActor *> ToDestroy;
	for(const auto &S : SquaresToDestroy)
		ToDestroy.Add(S.GetActor());
	SquaresToDestroy.Empty();
	for(AActor* D : ToDestroy)
		D->Destroy();
	for(auto& F : DropFunctions)
		F();
	DropFunctions.Empty();
}

