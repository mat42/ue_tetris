// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyTetris.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MyTetris, "MyTetris" );
