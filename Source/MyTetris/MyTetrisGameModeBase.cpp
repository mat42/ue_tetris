// Copyright Epic Games, Inc. All Rights Reserved.


#include "MyTetrisGameModeBase.h"
#include "Controls.h"

AMyTetrisGameModeBase::AMyTetrisGameModeBase()
{
	DefaultPawnClass = AControls::StaticClass();
}
