// Fill out your copyright notice in the Description page of Project Settings.

#include "Controls.h"
#include "Blocks.h"
#include <EngineUtils.h>

// Sets default values
AControls::AControls()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MoveCooldown = 0.05;
	FallCooldown = 1;
	ShortFallCooldown = 0.05;
}

// Called when the game starts or when spawned
void AControls::BeginPlay()
{
	Super::BeginPlay();
	// Find Blocks instance
	for(TActorIterator<ABlocks> It(GetWorld()); It; ++It)
	{
		AActor *Actor = *It;
		if(Actor->GetFName() == TEXT("Blocks"))
		{
			Blocks = reinterpret_cast<ABlocks*>(Actor);
		}
	}
	CurrentFallCooldown = FallCooldown;
}

// Called every frame
void AControls::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CurrentFallCooldown -= DeltaTime;
	CurrentAxisCooldown -= DeltaTime;
	if(Blocks && CurrentFallCooldown <= 0)
	{
		Blocks->MoveDown();
		CurrentFallCooldown = Accelerated? ShortFallCooldown: FallCooldown;
	}
}

// Called to bind functionality to input
void AControls::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveLeftRight", this, &AControls::MoveLeftRight);
	PlayerInputComponent->BindAction("MoveImmediate", EInputEvent::IE_Pressed, this, &AControls::ResetMoveCooldown);
	PlayerInputComponent->BindAxis("AccelerateDown", this, &AControls::AccelerateDown);
	PlayerInputComponent->BindAction("Rotate", EInputEvent::IE_Released, this, &AControls::Rotate);
	PlayerInputComponent->BindAction("Enter", EInputEvent::IE_Released, this, &AControls::Restart);
}

void AControls::MoveLeftRight(float AxisValue)
{
	if(Blocks && AxisValue && CurrentAxisCooldown <= 0)
	{
		Blocks->MoveLeftRight(AxisValue);
		CurrentAxisCooldown = MoveCooldown;
	}
}

void AControls::AccelerateDown(float AxisValue)
{
	Accelerated = AxisValue != 0;
}

void AControls::Rotate()
{
	if(Blocks) Blocks->Rotate();
}

void AControls::ResetMoveCooldown()
{
	CurrentAxisCooldown = 0;
}

void AControls::Restart()
{
	if(Blocks && Blocks->IsGameOver) Blocks->Restart();
}

