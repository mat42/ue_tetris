// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Controls.generated.h"

UCLASS()
class MYTETRIS_API AControls : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AControls();

	UPROPERTY()
	class ABlocks* Blocks;
	
	UPROPERTY(EditAnywhere, Category="Parameters")
	float FallCooldown;
	
	UPROPERTY(EditAnywhere, Category="Parameters")
	float MoveCooldown;
	
	UPROPERTY(EditAnywhere, Category="Parameters")
	float ShortFallCooldown;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void MoveLeftRight(float AxisValue);

	void AccelerateDown(float AxisValue);

	void Rotate();

	void ResetMoveCooldown();

	void Restart();

private:
	float CurrentFallCooldown;
	float CurrentAxisCooldown;
	bool Accelerated;
};
